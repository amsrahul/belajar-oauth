// import logo from './logo.svg';
import { useEffect, useState } from 'react';
import './App.css';
import jwtDecode from 'jwt-decode';

const google = window.google;

function App() {

  const [user, setUser] = useState({});

  function handleCallbackResponse(response) {
    console.log("Encoded JWT ID token:" + response.credential);
    const userObject = jwtDecode(response.credential);
    console.log(userObject); 
    setUser(userObject);
    document.getElementById("signInDiv").hidden = true;
  }

  function handleSignOut(event) {
    setUser({});
    document.getElementById("signInDiv").hidden = false;
    console.log(user);
  }

  useEffect(() => {
    google.accounts.id.initialize({
      client_id: "494095312758-1djdo121r7p9q8c2503d65crc3a32otm.apps.googleusercontent.com",
      callback: handleCallbackResponse
    });

    google.accounts.id.renderButton(
      document.getElementById("signInDiv"),
      {theme: "outline", size: "large"}
    )
  }, []);

  return (
    <div className="App">
      <div id="signInDiv"></div>
      { Object.keys(user).length != 0 &&
        <button onClick={(e)=> handleSignOut(e)}>Sign Out</button>
      }
      { user &&
        <div>
          <img src={user.picture} alt="profile-pict"></img>
          <h3>{user.name}</h3>
        </div>
      }
    </div>
  );
}

export default App;
